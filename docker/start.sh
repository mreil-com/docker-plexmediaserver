#!/bin/bash

PLEX_DIR="/plex"
REAL_DATA_DIR="${PLEX_DIR}/data"
mkdir -p "${REAL_DATA_DIR}"

DATA_DIR="${PLEX_DIR}/Plex Media Server"
ln -s "${REAL_DATA_DIR}" "${DATA_DIR}"
rm "${DATA_DIR}/plexmediaserver.pid"

export PLEX_MEDIA_SERVER_APPLICATION_SUPPORT_DIR=${PLEX_DIR}

cat "${DATA_DIR}/Preferences.xml"

PLEX_HOME=/usr/lib/plexmediaserver

export LD_LIBRARY_PATH=${PLEX_HOME}
${PLEX_HOME}/Plex\ Media\ Server &

sleep 5

tail -f /plex/Plex\ Media\ Server/Logs/*.log
